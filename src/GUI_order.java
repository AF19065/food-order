import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI_order {
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane Ordered;
    private JButton checkOutButton;
    private JPanel root;
    private JButton karaageButton;
    private JButton chahanButton;
    private JButton sobaButton;
    private JLabel totalYenLabel;

    int total = 0;
    void order(String food, int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "We have received your order for "
                                                                      + food + "! It will be served soon!");
            String currentText = Ordered.getText();
            Ordered.setText(currentText + food + " " + cost + "yen\n");
            total += cost;
            totalYenLabel.setText("Total         " + total + " yen     ");
        }
    }

    public GUI_order() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 200);
            }
        });
        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("Tempura.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 450);
            }
        });
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("Ramen.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 250);
            }
        });
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("Udon.jpeg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 150);
            }
        });
        karaageButton.setIcon(new ImageIcon( this.getClass().getResource("Karaage.jpg")
        ));
        chahanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chahan", 350);
            }
        });
        chahanButton.setIcon(new ImageIcon( this.getClass().getResource("Chahan.jpg")
        ));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba", 300);
            }
        });
        sobaButton.setIcon(new ImageIcon( this.getClass().getResource("Soba.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    int confirmation2 = JOptionPane.showConfirmDialog(null,
                            "Would you like to use a 100 yen discount coupon?",
                            "Coupon Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation2 == 0){
                        JOptionPane.showMessageDialog(null,
                                "100 yen discount coupon applied.");
                        if(total > 100){
                            total -= 100;
                        }
                        else{
                            total = 0;
                        }
                    }
                    JOptionPane.showMessageDialog(null,
                                                  "Thank you. The total price is " + total + " yen.");
                    Ordered.setText("");
                    total = 0;
                    totalYenLabel.setText("Total             " + total + " yen     ");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI_order");
        frame.setContentPane(new GUI_order().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
